# -*- coding: utf-8 -*-

from django.conf.urls import url
from django.contrib import admin
import news.views as views


admin.autodiscover()
urlpatterns = [
    url(r'blog.html/$', views.NewsListView.as_view(), name='news_list'),
    url(r'blog-inside.html/(?P<pk>[-\w]+)$', views.NewsDetailView.as_view(), name='news_detail'),
    url(r'home.html/$', views.MessageFormView.as_view(), name='send_message'),
    url(r'^$', views.MessageFormView.as_view(), name='index'),
]
