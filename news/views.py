# -*- coding: utf-8 -*-

from django.views.generic import ListView, DetailView, FormView
from news.forms import MessageForm
from news.models import News
from django.contrib import messages


class NewsListView(ListView):
    model = News
    template_name = 'news/news_list.html'
    paginate_by = 5

    def get_queryset(self):
        return News.objects.order_by('-created_at')


class NewsDetailView(DetailView):
    model = News
    template_name = 'news/news_detail.html'


class MessageFormView(FormView):
    template_name = 'news/send_message.html'
    form_class = MessageForm
    success_url = '/home.html/'

    def form_valid(self, form):
        form.save()
        messages.success(self.request,  u'form_saved')
        return super(MessageFormView, self).form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, u'form_invalid')
        return super(MessageFormView, self).form_invalid(form)


