# -*- coding: utf-8 -*-

from django import forms

from news.models import News, Image, Message, Choice
from ckeditor.widgets import CKEditorWidget


class MessageForm(forms.ModelForm):
    name = forms.CharField(label=u'мое имя',
                           widget=forms.TextInput(attrs={'placeholder': u'мое имя',
                                                         'class': 'main-form__input js-input'}))
    email = forms.EmailField(label=u'мой e-mail',
                             widget=forms.TextInput(attrs={'placeholder': u'мой e-mail',
                                                           'class': 'main-form__input js-input'}))
    phone = forms.CharField(label=u'мой телефон',
                            widget=forms.TextInput(attrs={'placeholder': u'мой телефон',
                                                          'class': 'main-form__input js-input'}))

    class Meta:
        model = Message
        fields = ('preferences', 'name', 'phone', 'email')

    def __init__(self, *args, **kwargs):
        super(MessageForm, self).__init__(*args, **kwargs)
        self.fields['preferences'].widget = forms.widgets.CheckboxSelectMultiple()
        self.fields['preferences'].label = u'хотелки'
        self.fields['preferences'].required = False
        self.fields['preferences'].queryset = Choice.objects.filter(active=True)


class NewsAdminForm(forms.ModelForm):
    text = forms.CharField(widget=CKEditorWidget())
    preview = forms.CharField(widget=forms.Textarea())

    class Meta:
        model = News
        fields = ('icon', 'title', 'preview', 'text', 'images')
