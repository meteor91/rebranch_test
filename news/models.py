# -*- coding: utf-8 -*-

from django.db import models
from django.conf import settings
from datetime import date
from django.db.models.signals import post_save, pre_delete
from utils import *


class Image(models.Model):
    image = models.ImageField(upload_to='user_images/'+str(date.today()), verbose_name=u'Изображение')
    author = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=u'Автор')
    title = models.CharField(max_length=255, null=True)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name=u'Дата создания')

    class Meta:
        verbose_name = u'Изображение'
        verbose_name_plural = u'Изображения'

    def __str__(self):
        return self.image.url

    def get_thumbnail_html(self):
        html = '<a class="image-picker" href="%s"><img src="%s" alt="%s"/></a>'
        return html % (self.image.url, get_thumbnail_url(self.image.url), self.title)
    get_thumbnail_html.short_description = u'Изображение'
    get_thumbnail_html.allow_tags = True


def post_save_handler(sender, **kwargs):
    create_thumbnail(kwargs['instance'].image.path)
post_save.connect(post_save_handler, sender=Image)


def pre_delete_handler(sender, **kwargs):
    delete_thumbnail(kwargs['instance'].image.path)
pre_delete.connect(pre_delete_handler, sender=Image)


class News(models.Model):
    title = models.CharField(max_length=255, verbose_name=u'Заголовок')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name=u'Дата создания')
    preview = models.CharField(max_length=511, verbose_name=u'Превью')
    text = models.TextField(verbose_name=u'Текст')
    author = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=u'Автор')
    images = models.ManyToManyField(Image, verbose_name=u'Изображение')
    icon = models.ForeignKey(Image, verbose_name=u'Иконка', related_name='icon', null=True)

    class Meta:
        verbose_name = u'Новость'
        verbose_name_plural = u'Новости'

    def __unicode__(self):
        return unicode(self.title)


class Choice(models.Model):
    label = models.CharField(max_length=31, verbose_name=u'Текст')
    active = models.BooleanField(default=True, verbose_name=u'Активный')
    author = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=u'Автор')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name=u'Дата создания')

    class Meta:
        verbose_name = u'Предпочтения'
        verbose_name_plural = u'Предпочтения'

    def __str__(self):
        return str(self.id)

    def __unicode__(self):
        return unicode(self.label)


class Message(models.Model):
    preferences = models.ManyToManyField(Choice, verbose_name=u'Предпочтения', null=True)
    name = models.CharField(max_length=31, verbose_name=u'Имя')
    phone = models.CharField(max_length=31, verbose_name=u'Телефон')
    email = models.CharField(max_length=31)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name=u'Дата создания')

    class Meta:
        verbose_name = u'Сообщение'
        verbose_name_plural = u'Сообщения'

    def __unicode__(self):
        return unicode(u'Сообщение от некоего '+self.name)