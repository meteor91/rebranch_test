# -*- coding: utf-8 -*-

from django.apps import AppConfig


class AppNameConfig(AppConfig):
    name = 'news'
    verbose_name = u'Блог'


default_app_config = 'news.AppNameConfig'