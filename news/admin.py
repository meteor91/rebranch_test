# -*- coding: utf-8 -*-

from django.contrib import admin
from news.forms import NewsAdminForm
from news.models import Image, News, Choice, Message


class ImageAdmin(admin.ModelAdmin):
    list_display = ['get_thumbnail_html', '__str__', 'title', 'created_at']
    list_display_links = ['__str__']
    list_filter = ['created_at']
    exclude = ['author']

    def save_model(self, request, obj, form, change):
        obj.author = request.user
        super(ImageAdmin, self).save_model(request, obj, form, change)


class NewsAdmin(admin.ModelAdmin):
    fields = ('icon', 'title', 'preview', 'images', 'text')
    list_display = ('title', 'preview', 'created_at', 'author')
    list_filter = ['created_at']
    form = NewsAdminForm

    def save_model(self, request, obj, form, change):
        obj.author = request.user
        super(NewsAdmin, self).save_model(request, obj, form, change)


class ChoiceAdmin(admin.ModelAdmin):
    fields = ('label', 'active')
    list_display = ('label', 'active', 'author', 'created_at')
    list_filter = ['created_at']

    def save_model(self, request, obj, form, change):
        obj.author = request.user
        super(ChoiceAdmin, self).save_model(request, obj, form, change)


class MessageAdmin(admin.ModelAdmin):
    fields = ('name', 'phone', 'email', 'preferences')
    list_display = ('name', 'phone', 'email', 'created_at')
    list_filter = ['created_at']

admin.site.register(Image, ImageAdmin)
admin.site.register(News, NewsAdmin)
admin.site.register(Choice, ChoiceAdmin)
admin.site.register(Message, MessageAdmin)
